﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileEngine
{
    class MapCell
    {
        private List<int> baseTiles = new List<int>();

        public List<int> BaseTiles
        {
            get { return baseTiles;}
            set { baseTiles = value; }
        }

        public int TileID
        {
            get { return BaseTiles.Count > 0 ? BaseTiles[0] : 0; }
            set
            {
                if (BaseTiles.Count > 0)
                {
                    BaseTiles[0] = value;
                }
                else
                {
                    AddBaseTile(value);
                }
            }
        }

        public void AddBaseTile(int tileID)
        {
            BaseTiles.Add(tileID);
        }

        public MapCell(int tileId)
        {
            TileID = tileId;
        }
    }
}
