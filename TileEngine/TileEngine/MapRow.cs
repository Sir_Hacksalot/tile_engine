﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileEngine
{
    class MapRow
    {
        private List<MapCell> columns = new List<MapCell>();

        public List<MapCell> Columns
        {
            get { return columns; }
            set { columns = value; }
        }
    }
}
